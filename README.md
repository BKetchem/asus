# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Full Asus Integration
* Version 1.0

### How do I get set up? ###

* There are 3 pieces to the ASUS Integration: TimeZones, AssetPusher, and JDE. For each piece install the appropriate sql scripts. 
* Re-point the database source as necessary
* Dependencies: The two assemblies for the asset pusher must be created with unrestricted access


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Chad Phillips : Brian Ketchem : Mark Harmon
* Timmons Group 2016