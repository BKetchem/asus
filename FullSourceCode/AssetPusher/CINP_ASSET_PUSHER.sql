USE [Cityworks_Dev]
GO
/****** Object:  StoredProcedure [dbo].[CINP_ASSET_PUSHER]    Script Date: 2/25/2016 10:14:48 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [dbo].[CINP_ASSET_PUSHER]

@FEATUREUID NVARCHAR(50)
,@WORKORDERID NVARCHAR(60)
,@DOM_ID DECIMAL(10,0)
 
as
 
declare	 @return_value		int
		,@returnval			nvarchar(max)
		,@xml				XML
		,@FEATTYPE			NVARCHAR(1)
		
		--ArcGIS REST Endpoint (only url_left and outfields should need to be edited)
		,@url_left_ONUS	    nvarchar(max) = 'http://agogisapp1:6080/arcgis/rest/services/ONUS_BR_WATER_SEWER_CTYW/ONUS_Basemap/MapServer/find?searchText='
		,@url_left_PSUS		nvarchar(max) = 'http://agogisapp1:6080/arcgis/rest/services/PSUS_JA_WATER_SEWER_CTYW/PSUS_Basemap/MapServer/find?searchText='
		,@url_left_ODUS		nvarchar(max) = 'http://agogisapp1:6080/arcgis/rest/services/ODUS_CTYW/ODUS_Cityworks_Master/MapServer/find?searchText='
		,@url_left_TUS		nvarchar(max) = 'http://agogisapp1:6080/arcgis/rest/services/TUS_AN_WATER_SEWER_CTYW/TUS_AN_WATER_SEWER_CTYW/MapServer/find?searchText='
		,@url_left_FBWS		nvarchar(max) = 'http://agogisapp1:6080/arcgis/rest/services/FBWSC_BL_WATER_SEWER_CTYW/FBWSC_BL_WATER_SEWER_CTYW/MapServer/find?searchText='
		,@url_search		nvarchar(max) = @FEATUREUID --'WV_56810'
		,@url_middle		nvarchar(max) = '&contains=true&searchFields=&sr=&layers='
		--,@url_layers		nvarchar(max) = '0%2C1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%2C9%2C10%2C11%2C12%2C13%2C14%2C15%2C16%2C17%2C18%2C19%2C20%2C21%2C22%2C23%2C24%2C25%2C26%2C27%2C28%2C29%2C30%2C31%2C32%2C33%2C34%2C35%2C36%2C37%2C38%2C39%2C40%2C41%2C42%2C43%2C44%2C45%2C46%2C47%2C48%2C49%2C50%2C51%2C52%2C53%2C54%2C55%2C56%2C57%2C58%2C59%2C60%2C61%2C62%2C63%2C64%2C65%2C66%2C67%2C68%2C69%2C70%2C71%2C72%2C73%2C74%2C75%2C76%2C77%2C78%2C79%2C80%2C81%2C82%2C83%2C84%2C85%2C86%2C87%2C88%2C89%2C90%2C91%2C92%2C93%2C94%2C95'
		,@url_layers		nvarchar(max) = '0%2C1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%2C9%2C10%2C11%2C12%2C13%2C14%2C15%2C16%2C17%2C18%2C19%2C20%2C21%2C22%2C23%2C24%2C25%2C26%2C27%2C28%2C29%2C30%2C31%2C32%2C33%2C34%2C35%2C36%2C37%2C38%2C39%2C40%2C41%2C42%2C43%2C44%2C45%2C46%2C47%2C48%2C49%2C50%2C51%2C52%2C53%2C54%2C55%2C56%2C57%2C58%2C59%2C60%2C61%2C62%2C63%2C64%2C65%2C66%2C67%2C68%2C69%2C70%2C71%2C72%2C73%2C74%2C75%2C76%2C77%2C78%2C79%2C80%2C81%2C82%2C83%2C84%2C85%2C86%2C87%2C88%2C89%2C90%2C91%2C92%2C93%2C94%2C95%2C96%2C97%2C98%2C99%2C100%2C101%2C102%2C103%2C104%2C105%2C106%2C107%2C108%2C109%2C110%2C111%2C112%2C113%2C114%2C115%2C116%2C117%2C118%2C119%2C120%2C121%2C122%2C123%2C124%2C125%2C126%2C127%2C128%2C129%2C130%2C131%2C132%2C133%2C134%2C135%2C136%2C137%2C138%2C139%2C140%2C141%2C142%2C143%2C144%2C145%2C146%2C147%2C148%2C149%2C150%2C151%2C152%2C153%2C154%2C155%2C156%2C157%2C158%2C159%2C160%2C161%2C162%2C163%2C164%2C165%2C166%2C167%2C168%2C169%2C170%2C171%2C172%2C173%2C174%2C175%2C176%2C177%2C178%2C179%2C180%2C181%2C182%2C183%2C184%2C185%2C186%2C187%2C188%2C189%2C190%2C191%2C192%2C193%2C194%2C195%2C196%2C197%2C198%2C199%2C200%2C201%2C202%2C203%2C204%2C205%2C206%2C207%2C208%2C209%2C210'
		,@url_right			nvarchar(max) = '&layerDefs=&returnGeometry=true&maxAllowableOffset=&geometryPrecision=&dynamicLayers=&returnZ=false&returnM=false&gdbVersion=&f=pjson'
		,@url_final			nvarchar(max)
		,@text6_ins			nvarchar(max)
		,@unit_type			nvarchar(2)

		,@ErrorMessage		nvarchar(max)

begin 
	BEGIN TRY

		--if @DOM_ID = 5
		--begin
		--   SET @FEATTYPE = SUBSTRING(@FEATUREUID,CHARINDEX('_',@FEATUREUID) + 1, 1)
		--   select 'FEATTYPE =' + @feattype
		--end
		--else if @DOM_ID = 3
		--begin
		--  SET @FEATTYPE = SUBSTRING(@FEATUREUID,CHARINDEX('_',@FEATUREUID) + 1, 1)
		--  select 'FEATTYPE =' + @feattype
		--end
		--else if @DOM_ID = 2
		--begin
		--  SET @FEATTYPE = SUBSTRING(@FEATUREUID,CHARINDEX('_',@FEATUREUID) + 1, 1)
		--  select 'FEATTYPE =' + @feattype
		--end
		--else if @DOM_ID = 4
		--begin
		--  SELECT @FEATTYPE = SUBSTRING(@FEATUREUID, 1, 1)
		--  select 'FEATTYPE =' + @feattype
		--end
		--else if @DOM_ID = 1
		--begin
		--  SELECT @FEATTYPE = SUBSTRING(@FEATUREUID, 1, 1)
		--  select 'FEATTYPE =' + @feattype
		--end

		--AFTER ALL OF THE MAP SERVICES HAVE BEEN UPDATED, THIS WILL REPLACE THE ABOVE IF STATEMENTS
		SET @FEATTYPE = SUBSTRING(@FEATUREUID,CHARINDEX('_',@FEATUREUID) + 1, 1)

			--build url string
		IF @FEATTYPE = 's' AND @DOM_ID = 1
		begin
			set @url_final = @url_left_ONUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'WW'
		end
		else if @FEATTYPE = 'w' AND @DOM_ID = 1
		begin 
			set @url_final = @url_left_ONUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'W'
		end
-------------------------------------------------------------------------------------------------------------

		else if @FEATTYPE = 's' AND @DOM_ID = 4
		begin
			set @url_final = @url_left_PSUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'WW'
		end
		else if @FEATTYPE = 'w' AND @DOM_ID = 4
		begin 
			set @url_final = @url_left_PSUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'W'
		end
----------------------------------------------------------------------------------------------------------------------

		else if @FEATTYPE = 's' AND @DOM_ID = 2
		begin
			set @url_final = @url_left_ODUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'WW'
		end
		else if @FEATTYPE = 'w' AND @DOM_ID = 2
		begin 
			set @url_final = @url_left_ODUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'W'
		end
------------------------------------------------------------------------------------------------------------------------

		else if (@FEATTYPE = 's' or @FEATTYPE = 'AN_W') AND @DOM_ID = 5
		begin
			set @url_final = @url_left_TUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'WW'
		end
		else if @FEATTYPE = 'w' AND @DOM_ID = 5
		begin 
			set @url_final = @url_left_TUS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'W'
		end
----------------------------------------------------------------------------------------------------------------------------

		else if @FEATTYPE = 's' AND @DOM_ID = 3
		begin
			set @url_final = @url_left_FBWS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'WW'
		end
		else if @FEATTYPE = 'w' AND @DOM_ID = 3
		begin 
			set @url_final = @url_left_FBWS + @url_search+ @url_middle + @url_layers + @url_right
			select @unit_type = 'W'
		end
-----------------------------------------------------------------------------------------------------------------------------------


		SELECT @url_final
	
		--hit endpoint and convert json to xml 
		exec [dbo].[CINP_RESTJSONTOXML] @url_final, @returnval OUTPUT
		
		select @text6_ins = unit_code from [dbo].[CLUT_BUS_UNIT]
		where UNIT_NAME = @returnval and UNIT_TYPE = @unit_type
		
		UPDATE azteca.WORKORDER
		SET TEXT6 = @text6_ins
		WHERE WORKORDERID = @WORKORDERID

		INSERT INTO DBO.TESTAP2_CANDELETE
		VALUES (@WORKORDERID, @FEATTYPE, @url_final, @text6_ins, @returnval, SYSDATETIME(), @DOM_ID)

	END TRY
	BEGIN CATCH
		--RAISERROR('ERROR RAISED IN STORED PROCEDURE CINP_ASSET_PUSHER', 16, 1)
		SET @ErrorMessage = ERROR_MESSAGE() + ' ' + CAST(ERROR_LINE() AS nvarchar)
		RAISERROR(@ErrorMessage , 16,1)
	END CATCH
	
end