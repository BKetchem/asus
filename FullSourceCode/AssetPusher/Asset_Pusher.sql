USE [Cityworks_Dev]
GO
/****** Object:  Trigger [azteca].[ASSET_PUSHER]    Script Date: 2/25/2016 10:04:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BRIAN KETHCEM
-- Create date: 20151119
-- Description:	PART OF THE ASSET PUSHER INTEGRATION TAKES THE INSERTED ASSET AND WORK ORDER ID AND CALLS A SP
-- =============================================
ALTER TRIGGER [azteca].[ASSET_PUSHER]
   ON  [azteca].[WORKORDERENTITY]
   After INSERT, UPDATE
AS
	DECLARE @INS_WOID NVARCHAR(60), @INS_FEATUID NVARCHAR(50)
	,@ERRORSTRING NVARCHAR(MAX), @DOM_ID DECIMAL (10,0)
BEGIN
	---- SET NOCOUNT ON added to prevent extra result sets from
	---- interfering with SELECT statements.
	SET NOCOUNT ON
	SELECT @INS_WOID = WORKORDERID, @INS_FEATUID = FEATUREUID FROM inserted
	select @DOM_ID = DOMAINID From Azteca.WORKORDER where WORKORDERID = @INS_WOID
	IF @INS_FEATUID <> ''
	BEGIN
		exec [dbo].[CINP_ASSET_PUSHER] @INS_FEATUID, @INS_WOID, @DOM_ID
	END
	--select * from azteca.WORKORDER
END

