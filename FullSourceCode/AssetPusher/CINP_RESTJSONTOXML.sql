USE [Cityworks_Dev]
GO

/****** Object:  StoredProcedure [dbo].[CINP_RESTJSONTOXML]    Script Date: 2/25/2016 10:16:31 AM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[CINP_RESTJSONTOXML]
	@weburl [nvarchar](4000),
	@returnval [nvarchar](max) OUTPUT
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [RestJsonToXML].[StoredProcedures].[GetAssetInfo]
GO


