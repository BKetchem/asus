USE [Cityworks_Dev]
GO

/****** Object:  Trigger [azteca].[SetCALLDateInsert]    Script Date: 2/25/2016 9:44:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		BRIAN KETCHEM
-- Create date: 20160126
-- Description:	UPDATE DATE TO MATCH DOMAIN
-- =============================================
CREATE TRIGGER [azteca].[SetCALLDateInsert]
   ON  [azteca].[CUSTOMERCALL]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for trigger here
	DECLARE @REQID DECIMAL(10,0),
			@DOMAINID DECIMAL (10,0),
			@INS_DATE DATETIME,
			@INCIDENTNUM DECIMAL(10,0),
			@OUT_DATE DATETIME,
			@HOURDIFF INT
	SELECT @REQID = REQUESTID, @INS_DATE = DATETIMECALL, @INCIDENTNUM = INCIDENTNUM FROM inserted
	SET @DOMAINID = (SELECT DOMAINID FROM AZTECA.REQUEST WHERE REQUESTID = @REQID)
	IF (@DOMAINID = '3')
		SET @HOURDIFF = 1
	ELSE 
		SET @HOURDIFF = 3

	SET @OUT_DATE = DATEADD(HOUR, @HOURDIFF, @INS_DATE)

	UPDATE azteca.CUSTOMERCALL
	SET DATETIMECALL = @OUT_DATE
	WHERE INCIDENTNUM = @INCIDENTNUM
END

GO


